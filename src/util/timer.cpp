// -*- coding:utf-8; tab-width:4; mode:cpp -*-
// Copyright (C) 2014  ISAAC LACOBA MOLINA
// author: Isaac Lacoba Molina
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "timer.h"

Timer::Timer() {
  last_time_ = std::chrono::steady_clock::now();
}

Timer::~Timer() { }

float
Timer::get_delta_time() {
  Timer::DeltaTime delta = std::chrono::steady_clock::now() -
     last_time_;
  last_time_ = std::chrono::steady_clock::now();

  return  std::chrono::duration_cast<std::chrono::duration<float>>(delta).count();
}
