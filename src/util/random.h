#ifndef RANDOM_H
#define RANDOM_H
#include <random>
#include <chrono>

class Random {
  std::default_random_engine generator_;
public:
  Random();
  virtual ~Random();
  double real(double lower, double higher);

private:
    float seed();
};



#endif
