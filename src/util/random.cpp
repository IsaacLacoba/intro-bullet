// -*- coding:utf-8; tab-width:4; mode:cpp -*-
#include "random.h"

Random::Random() {
  generator_ =
    std::default_random_engine(seed());
}

Random::~Random() {

}

double
Random::real(double lower, double higher) {
  return  std::uniform_real_distribution<>{lower, higher}(generator_);
}

float
Random::seed() {
 return std::chrono::high_resolution_clock::now().
                     time_since_epoch().count();
}
