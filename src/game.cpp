// -*- coding:utf-8; tab-width:4; mode:cpp -*-
// Copyright (C) 2014  ISAAC LACOBA MOLINA
// author: Isaac Lacoba Molina
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "game.h"

Game::Game() {
  scene_ = std::make_shared<Scene>();
  physics_ = std::make_shared<Physics>();
  input_ = std::make_shared<EventListener>(scene_->window_);
  sphere_shape = static_cast<btSphereShape*>(physics_->create_shape(btScalar(5)));
}

Game::~Game() {
}

void
Game::start() {
  create_ground();
  register_hooks();
  game_loop();
}

void
Game::game_loop() {
  float delta_time;

  while(!input_->exit_) {
    delta_time += timer_.get_delta_time();
    input_->capture();
    if(delta_time >= 1/60) {
      input_->check_events();
      physics_->step_simulation(delta_time, 32);
      scene_->render_one_frame();
      delta_time = 0.f;
    }
  }
}

void
Game::register_hooks() {
  input_->add_hook({std::make_pair(OIS::KC_ESCAPE, true)}, EventType::menu,
                   std::bind(&EventListener::shutdown, input_));
  input_->add_hook({std::make_pair(OIS::KC_B, true)}, EventType::menu,
                   std::bind(&Game::create_sphere, this));
}

void
Game::create_ground() {
  std::string name = "ground";
  std::string mesh = "ground";
  Ogre::Vector3 position(0, 0 ,0);
  Ogre::SceneNode* ground = scene_->create_ground(position, name, mesh);

  btQuaternion rotation = btQuaternion(0, 0, 0, 1);
  btVector3 translation = btVector3(0, -1, 0);
  btTransform transform = btTransform( rotation, translation);

  int mass = 0;
  btScalar distance_to_origin = 1;
  btVector3 coordinates(0, 1, 0);
  btCollisionShape* ground_shape =
    physics_->create_shape(coordinates, distance_to_origin);

  ball_bodies.push_back(physics_->
    create_rigid_body(transform, ground, ground_shape, mass));
  ball_bodies.back()->setRestitution(0.8f);
  ball_bodies.back()->setFriction(0.6f);
}

void
Game::create_sphere() {
  create_graphic_element();
  create_physic_element();
}

void
Game::create_graphic_element() {
  std::string name = "ball" + std::to_string(ball_nodes.size());

  ball_nodes.
    push_back(scene_->create_graphic_element("",
                                             name,
                                             "ball.mesh"));
  ball_nodes.back()->setScale(5.0, 5.0, 5.0);
}

void
Game::create_physic_element() {
  btQuaternion rotation = btQuaternion(btVector3(1, 1, 1),
                                       random_.real(0.f, 90.f));
  btVector3 translation = btVector3(0, 100, 0);
  btTransform transform = btTransform( rotation, translation);

  int mass = 80;

  ball_bodies.push_back(physics_->
    create_rigid_body(transform, ball_nodes.back(), sphere_shape, mass));
  ball_bodies.back()->setRestitution(0.8f);
  ball_bodies.back()->setFriction(1.5f);
}
