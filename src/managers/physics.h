// author: Isaac Lacoba Molina
// Copyright (C) 2014  ISAAC LACOBA MOLINA
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#ifndef PHYSICS_H
#define PHYSICS_H
#include "motionstate.h"

#include <OgreSceneNode.h>
#include <btBulletDynamicsCommon.h>


class Physics {
  btVector3 gravity_;

  btBroadphaseInterface* broadphase_;
  btSequentialImpulseConstraintSolver* solver_;
  btDefaultCollisionConfiguration* collision_configuration_;
  btCollisionDispatcher* dispatcher_;

 public:
  typedef std::shared_ptr<Physics> shared;
  btDiscreteDynamicsWorld* dynamics_world_;

  Physics(btVector3 gravity = btVector3(0, -100, 0));
  virtual ~Physics();
  btRigidBody* create_rigid_body(const btTransform &worldTransform,
                                 Ogre::SceneNode* node,
                                 btCollisionShape* shape,
                                 btScalar mass);
  btCollisionShape* create_shape(btVector3 halfExtent);
  btCollisionShape* create_shape(btScalar  radius);
  btCollisionShape* create_shape(btVector3 coordinates,
                                 btScalar distance_to_origin);
  btCompoundShape* create_compound_shape(btVector3 origin, btCollisionShape* child);
  void step_simulation(float deltaT, int maxSubSteps);

  void load_mesh(std::string file);

};

#endif
