// -*- coding:utf-8; tab-width:4; mode:cpp -*-
// Copyright (C) 2014  ISAAC LACOBA MOLINA
// author: Isaac Lacoba Molina
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "physics.h"

Physics::Physics(btVector3 gravity) {
  gravity_ = gravity;

  broadphase_ = new btDbvtBroadphase();

  solver_ = new btSequentialImpulseConstraintSolver();
  collision_configuration_ = new btDefaultCollisionConfiguration();
  dispatcher_ = new btCollisionDispatcher(collision_configuration_);
  dynamics_world_ = new btDiscreteDynamicsWorld(dispatcher_,
                        broadphase_, solver_, collision_configuration_);
    dynamics_world_->setGravity(gravity_);
}

Physics::~Physics() {
  delete dynamics_world_;
  delete dispatcher_;
  delete collision_configuration_;
  delete solver_;
  delete broadphase_;
}

btRigidBody*
Physics::create_rigid_body(const btTransform &worldTransform,
                  Ogre::SceneNode* node,
                  btCollisionShape* shape,
                  btScalar mass){
  btVector3 inertia(0 ,0 ,0);
  if(mass != 0)
    shape->calculateLocalInertia(mass, inertia);

  MotionState* motionState = new MotionState(worldTransform, node);
  btRigidBody::btRigidBodyConstructionInfo
    rigidBodyCI(mass, motionState, shape, inertia);

  btRigidBody* rigidBody = new btRigidBody(rigidBodyCI);
  dynamics_world_->addRigidBody(rigidBody);

  return rigidBody;
}

btCollisionShape*
Physics::create_shape(btVector3 halfExtent){
  return new btBoxShape(halfExtent);
}

btCollisionShape*
Physics::create_shape(btScalar radius){
  return new btSphereShape(radius);
}

btCollisionShape*
Physics::create_shape(btVector3 coordinates,
                      btScalar distance_to_origin) {
  return new btStaticPlaneShape(coordinates,
                                distance_to_origin);
}

btCompoundShape*
Physics::create_compound_shape(btVector3 origin, btCollisionShape* child){
  btCompoundShape* compound = new btCompoundShape();
  btTransform localTrans;
  localTrans.setIdentity();
  localTrans.setOrigin(origin);

  compound->addChildShape(localTrans, child);
  return compound;
}


void
Physics::step_simulation(float deltaT, int maxSubSteps) {
  dynamics_world_->stepSimulation(deltaT, maxSubSteps);
}
