#ifndef GAME_H
#define GAME_H
#include <memory>
#include <vector>

#include "scene.h"
#include "physics.h"
#include "input.h"
#include "random.h"
#include "timer.h"

class Game {
  Random random_;
  Timer timer_;

  int balls_;
  std::vector<Ogre::SceneNode*> ball_nodes;
  std::vector<btRigidBody*> ball_bodies;
  btSphereShape* sphere_shape;

  Scene::shared scene_;
  Physics::shared physics_;
  EventListener::shared input_;

public:
  Game();
  virtual ~Game();

  void start();

private:
  void game_loop();
  void create_ground(Ogre::Vector3 position, std::string name,
                     std::string mesh, Ogre::Degree angle);
  void create_ground();
  void create_sphere();
  void create_graphic_element();
  void create_physic_element();
  void register_hooks();
};


#endif
