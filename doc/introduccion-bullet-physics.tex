\documentclass[a4paper,10pt]{article}
\usepackage[spanish]{babel}
\usepackage[utf8]{inputenc}
\usepackage{anysize} % Soporte para el comando \marginsize
\usepackage{listings}
\usepackage{formular}
\usepackage{enumerate}
\usepackage[pdftex]{graphicx}
\usepackage[colorlinks,linkcolor=black,citecolor=black]{hyperref}

\DeclareGraphicsExtensions{.pdf,.png,.jpg}

\usepackage{color}
\definecolor{gray97}{gray}{.97}
\definecolor{gray75}{gray}{.75}
\definecolor{gray45}{gray}{.45}

\lstset{ frame=Ltb,
     framerule=0pt,
     aboveskip=0.5cm,
     framextopmargin=3pt,
     framexbottommargin=3pt,
     framexleftmargin=0.4cm,
     framesep=0pt,
     rulesep=.4pt,
     backgroundcolor=\color{gray97},
     rulesepcolor=\color{black},
     %
     stringstyle=\ttfamily,
     showstringspaces = false,
     basicstyle=\small\ttfamily,
     commentstyle=\color{gray45},
     keywordstyle=\bfseries,
     %
     numbers=left,
     numbersep=15pt,
     numberstyle=\tiny,
     numberfirstline = false,
     breaklines=true,
   }

% minimizar fragmentado de listados
\lstnewenvironment{listing}[1][]
   {\lstset{#1}\pagebreak[0]}{\pagebreak[0]}

\lstdefinestyle{consola}
   {basicstyle=\scriptsize\bf\ttfamily,
    backgroundcolor=\color{gray75},
   }

\lstdefinestyle{C++}
   {language=C++,
   }

\renewcommand*\lstlistingname{Listado}

\marginsize{3cm}{3cm}{2.5cm}{2.5cm}

\title{Introducción a Bullet Physics}
\author{Isaac Lacoba Molina}

\begin{document}
\thispagestyle{empty}
\begin{titlepage}

  \begin{figure}[t!]
    \begin{center}
      \includegraphics[width=8cm]{figs/bullet-logo.png}
    \end{center}
  \end{figure}
  \vspace*{0.7in}

  \centering
  \Huge{\textbf{Introduccion a Bullet Physics}}\\

  \rule{135mm}{0.1mm}\\

  \centering
 \begin{Large}
    \textbf{Autor:} Isaac Lacoba Molina\\
    \textbf{Twitter:} @IsaacLacoba \\
    \textbf{Correo:} Isaac.Lacoba@gmail.com \\
    \date{today}
  \end{Large}


\end{titlepage}

\thispagestyle{empty}

\newpage

\tableofcontents

\newpage

\section{Introducción}
\label{Introduction}

Este documento pretende servir como una pequeña introducción a Bullet
Physics. En él, se explicará brevemente las características mas
importantes de la biblioteca, pasando a continuación a ver algo de
código fuente que permita comprender como manejar los elementos
básicos de la misma. Todos los ejemplos dados en este documento
estarán orientados al desarrollo de videojuegos.\\

En el ejemplo, se usará como biblioteca de renderizado Ogre3D versión
1.8, OIS versión 1.3 para gestionar eventos de teclado, Bullet versión
2.82 y como sistema operativo Debian.\\

Si el lector no está familiarizado con el uso de Ogre3D dispone de un
tutorial en el siguiente link: \url{http://goo.gl/btbPOS}.\\


\section{¿Qué es Bullet?}
\label{sec:que-es-bullet}

Bullet Physics~\cite{bullet-web} es una biblioteca de físicas y
detección de colisiones. Se distribuye bajo licencia ZLib y está
desarrollada usando el lenguaje de programación C++. El código fuente
se encuentra disponible en el repositorio oficial del
proyecto~\cite{bullet-repo}. Ofrece soporte para una gran multitud de
plataformas, tales como PlayStation 3 y 4, Xbox 360 y One, Wii,
Gnu/Linux, Windows, MacOSX, iPhone, Android y navegador
web(chrome)~\cite{bullet-chrome}.\\

Bullet ha sido usado en multitud de películas, tales como Hancock o
Sherlock Holmes, así como videojuegos comerciales entre los que
destacan Grand Theft auto IV, Grand Theft auto V o Red Dead
Redemption. Además, la NASA está utilizando Bullet~\cite{bullet-nasa}
en un framework propio de cálculo de integridad
tensional~\footnote{del inglés \emph{tensegrity}} en robots.\\

La principal tarea de un motor de físicas es la de detectar colisiones
entre cuerpos físicos, además de aplicar transformaciones geometrías y restricciones
físicas a estós. En cualquier videojuego se hace necesario el uso de un motor de
físicas, por una serie de razones:
%%
\begin{itemize}
\item Si no controlamos las colisiones entre los cuerpos, estos se
  atravesarían entre sí, quitando realismo.

\item Parte de la mecánica de juego puede consistir en hacer chocar
  unos objetos con otros. Por enumerar algún juego de estas
  características tenemos \emph{Arkanoid}, donde la pelota debe
  chocar contra los ladrillos o \emph{Bubble Bobble}, donde el dragon
  protagonista suelta burbujas con las que derrota a los enemigos.

\item Puede ser necesario modelar propiedades físicas; por ejemplo, en
  juegos de carreras de coches, a la hora de realizar derrapes, es
  necesario simular la perdida de fricción que sufren los neumáticos.
\end{itemize}

Gracias al uso de una biblioteca de físicas es posible crear juegos
mas realistas, al tiempo que abstrae al desarrollador de los detalles
de bajo nivel. Entre las características mas importantes de Bullet
podemos enumerar:
\begin{itemize}
\item Detección de colisiones, tanto continua como discreta,
  incluyendo rayqueries y tests de colisión de formas
  convexas (\emph{sweep test}). Los test de colisión permiten mallas
  convexas y cóncavas, además de todo tipo de formas primitivas.
\item Implementa dinámica de cuerpos rígidos, de vehículos,
  controladores de personajes, restricción de giro para
  ragdolls~\cite{ragdoll}, restricciones de tipo slider, bisagra y
  6DOF~\cite{del inglés ``six degrees of freedom}.
\item Incluye dinámica de cuerpos fluidos para ropa, tela y volumenes
  deformables con dos formas de interacción con cuerpos rígidos,
  incluyendo soporte de restricciones.
\item Existen plugins para dar soporte a Maya, está integrado con
  Blender además de soportar ficheros COLLADA.
\end{itemize}

\subsection{Arquitectura}

Bullet ha sido diseñado para ser modular y adaptable. La biblioteca da
la libertad al desarrollador de usar los componentes que necesite en
cada momento, ignorando los demás. Por ejemplo, se podría hacer uso de
la capa de detección de colisiones sin hacer uso de las capas
superiores o viceversa, hacer uso de la capa de dinámica de cuerpos
rígidos ignorando las capas inferiores. En la
figura~\ref{fig:organizacion} se
puede observar un esquema general de la organización por capas de la biblioteca.\\

\begin{figure}[h]
  \centering
  \includegraphics[width=8cm]{figs/organizacion-bullet.png}
  \caption{Organización por capas de Bullet}
  \label{fig:organizacion}
\end{figure}


\subsection{Encauzamiento del componente de física de cuerpos rígidos}

El siguiente diagrama muestra las estructuras de datos mas
importantes, así como las etapas del encauzamiento dentro de
Bullet. Este encauzamiento se ejecuta de izquierda a derecha,
comenzando por aplicar la gravedad y terminando por integrar las
posiciones de los cuerpos.

\begin{figure}[h]
  \centering
  \includegraphics[width=16cm]{figs/pipeline-bullet.png}
  \caption{Pipeline del componente de cuerpos }
  \label{fig:pipeline}
\end{figure}

El encauzamiento y las estructuras de datos están representados en
Bullet a través de la clase DynamicsWorld. Cuando se ejecuta el método
"stepSimulation de dicha clase, en realidad se está ejecutando el
encauzamiento anterior. La implementación por defecto se encuentra en
la clase btDiscreteDynamicsWorld.\\

Bullet permite al desarrollador trabajar con subfases del
encauzamiento, como la de detección de colisiones, la fase en la que
se aplican los efectos de las colisiones a los cuerpos
físicos(narrowphase) o la fase de resolución de restricciones.\\

\section{Veamos algo de código}
A continuación veremos un sencillo ejemplo en el que generaremos
esferas que rebotarán contra el suelo. Estas esferas se crearán
dinámicamente al pulsar la tecla 'B'.\\

Usaremos la biblioteca OIS~\cite{web-ois} para gestionar los eventos
de teclado y Ogre3D como motor de renderizado, que nos permitirá
representar la escena. En este artículo se dará por supuesto los
conceptos básicos de Ogre3D. Para el lector que no esté acostumbrado a
trabajar con esta biblioteca, se aconseja conocer al menos como
inicializar el motor y cómo gestionar nodos de escena y entidades. El
código fuente se puede encontrar en el repositorio de este
proyecto~\cite{repo-ejemplo}.

Pero antes de empezar veremos cómo instalar las bibilotecas que vamos
a necesitar.

\subsection{Instalación del software}
\label{sec:inst-del-softw}

Como se dijo en la introducción en este tutorial estamos utilizando
Debian. En el repositorio de este tutorial podemos encontrar un
fichero \href{http://goo.gl/cKHhq9}{DEPENDS} en el que se listan todos
los paquetes necesarios. En caso de distribuciones GNU/linux
diferentes a Debian, es posible que no se encuentren disponibles las
mismas versiones de los paquetes, aunque no debería haber problemas a
la hora de compilar y ejecutar el ejemplo. \\

Para instalar las bibliotecas necesarias, tan solo tendremos que
ejecutar el siguiente comando en un terminal:

\begin{listing}[style=bash]
  sudo apt-get install libois-1.3.0 libois-dev
  sudo apt-get install libogre-1.8.0 libogre-1.8-dev
  sudo apt-get install libbullet-dev libbullet2.82-dbg libbullet-extras-dev
\end{listing}

El primero de los comandos instalará OIS, la bibilioteca de gestión de
eventos de entrada, el segundo instalará Ogre3D, la biblioteca de
renderizado y ,por último, el tercero instalará Bullet.\\

Hecho esto, podemos comenzar a crear nuestro ejemplo.

\subsection{Inicialización de Bullet}
En nuestro ejemplo, todo lo relativo a la inicialización de Bullet se
puede encontrar en los ficheros \emph{physics.cpp} y
\emph{physics.h}. En él, se van a modelar las esferas usando cuerpos
rígidos. Los cuerpos rígidos tienen como principal característica que
no se deforman; es decir, la distancia entre cualquier par de vertices
que forman la malla nunca varía. Por el contrario, los cuerpos fluidos
son aquellos que permiten deformaciones. En este artículo se va a
intentar obviar toda la base matemática y se va a centrar la
explicación en los aspectos referentes a la biblioteca. \\

El elemento mas importante en Bullet es el \emph{Mundo}. El \emph{Mundo}
dentro de Bullet tiene varias responsabilidades, entre las que
podemos destacar:
\begin{itemize}
\item servir como estructura de datos donde almacenar los cuerpos
  físicos que lo conforman.
\item aplicar una serie de restricciones a estos cuerpos, como la
  fuerza de la gravedad, detectar y aplicar colisiones entre estos
  cuerpos y actualizar su posición automáticamente cuando se aplique
  cualquier tipo de fuerza sobre estos.
\end{itemize}

El \emph{Mundo} tiene diversas implementaciones dentro de la
bibilioteca, dependiendo de si utilizamos cuerpos rígidos o fluidos.
En nuestro caso se usan cuerpos rígidos, de modo que la clase que
utilizaremos será \emph{btDiscreteDynamicsWorld}. En el siguiente
listado de código vemos cómo inicializar el objeto
\emph{btDiscreteDynamicsWorld}:
\begin{listing}[style=C++]
    btVector3 worldMin(-1000,-1000,-1000);
    btVector3 worldMax(1000,1000,1000);

    broadphase_ = new btAxisSweep3(worldMin,worldMax);

    solver_ = new btSequentialImpulseConstraintSolver();
    collisionConfiguration_ = new btDefaultCollisionConfiguration();
    dispatcher_ = new btCollisionDispatcher(collisionConfiguration_);

    dynamics_world_ = new btDiscreteDynamicsWorld(dispatcher_,
                        broadphase_, solver_, collisionConfiguration_);
    dynamics_world_->setGravity(gravity_);

\end{listing}

Cada uno de los objetos que recibe el constructor de la clase
\emph{btDiscreteDynamicsWorld} corresponde con una de las fases del
pipeline físico. El objeto broadphase\_ corresponde a la fase de
detección de colisiones. Existen tres implementaciones de esta fase
que trabajan con diferentes estructuras de datos, lo que permite
acelerar la busqueda de pares de colisión dependiendo de nuestras
necesidades:
\begin{itemize}
\item btDbvtBroadphase usa una jerarquía de volumenes delimitantes
  basada en un arbol AABB.
\item btAxisSweep3 y bt32BitAxisSweep3 implementa  un algoritmo 3d de barrido y poda.
\item btCudaBroadphase implementa un grid usando el hardware de la
  GPU. Hace uso de Cuda, una tecnología para tarjetas gráficas Nvidia.
\end{itemize}

La fase de detección de colisiones hace uso de una serie de estructuras de datos:

\begin{itemize}
\item \emph{btCollisionObject}: almacena formas de colisión y las
  transformaciones de éstas.
\item \emph{btCollisionShape}: describe la forma de colisión de un
  objeto de colisión, tal como una caja, una esfera, una forma convexa(convex
  hull) o una malla de triángulos. Una forma de
  colisión puede ser compartida entre
  múltiples objetos de colisión.
\item \emph{btGhostObject}: es un caso especial de cuerpo de colisión
  útil para realizar consultas de colisión de una forma rápida.
\item \emph{btCollisionWorld}: almacena todos los objetos de colisión
  y proporciona una interfaz que permite realizar peticiones de forma
  eficiente. Nuestro objeto dynamics\_world\_ es de tipo
  btDiscreteDynamicsWorld, que es una subclase de ésta. \end{itemize}

El objeto dispatcher\_ pertenece a la clase btCollisionDispatcher, la
cuál ofrece algoritmos que manejan pares de colisión ConvexConvex y
ConvexConcave. Esta clase corresponde a la etapa narrowphase, usando
los pares de colisión generados en la fase Broadphase.\\

El objeto collisionConfiguration\_, de la clase
collisionbtDefaultCollisionConfiguration, ofrece una configuración por
defecto que define aspectos internos del asignador de la pila de
detección de colisiones, del asignador de la pila de memoria de
bullet, etc.\\

Por último, el objeto solver\_, de la clase
btSequentialImpulseConstraintSolver, ofrece una implementación del
método Gauss-Seidel usado en analisis numérico para resolución de
sistemas de ecuaciones lineales. Esta clase está implementada usando
usando SIMD(\emph{Single Instruction, Multiple Data}), una técnica
utilizada para conseguir paralelismo a nivel de datos. Sin entrar en
detalles, consiste en aplicar una misma operación sobre un conjunto de
datos.\\

Tras inicializar la biblioteca, el siguiente paso consiste en crear
cuerpos y formas de colisión.\\

\subsection{Creando los cuerpos rígidos de las esferas}
El siguiente listado de código muestra cómo se crean los cuerpos
rígidos en nuestro ejemplo:

\begin{listing}[style=C++]
btRigidBody*
Physics::create_rigid_body(const btTransform &worldTransform,
                  Ogre::SceneNode* node,
                  btCollisionShape* shape,
                  btScalar mass){
  btVector3 inertia(0 ,0 ,0);

  if(mass != 0)  shape->calculateLocalInertia(mass, inertia);

  MotionState* motionState = new MotionState(worldTransform, node);
  btRigidBody::btRigidBodyConstructionInfo
    rigidBodyCI(mass, motionState, shape, inertia);

  btRigidBody* rigidBody = new btRigidBody(rigidBodyCI);
  dynamics_world_->addRigidBody(rigidBody);

  return rigidBody;
}
\end{listing}

El constructor de la clase \emph{btRigidBody} recibe un objeto
\emph{btRigidBody::btRigidBodyConstructionInfo}. Este objeto sirve
para inyectar al constructor de la clase información relativa al
cuerpo rígido que se va a crear . Los datos que se le pasan son la
masa del objeto, el estado del cuerpo(\emph{Motion State} del cuál
hablaremos
mas adelante), la forma física del cuerpo y la inercia.\\

En nuestro método, comprobamos que la masa del cuerpo sea distinta de
cero, ya que Bullet interpreta que un cuerpo sin masa es lo mismo que
un cuerpo con masa infinita; es decir, es inamovible. A partir de la
forma del cuerpo y de la masa de este, Bullet calcula la inercia del
cuerpo físico que estamos construyendo a través del método
\emph{calculateLocalInertia}. Una vez creado el cuerpo, hay que
añadirlo al mundo(dynamics\_world\_\rightarrow
addRigidBody(rigidBody)). \\

Bullet ofrece unas cuantas formas primitivas de colisión:
\begin{itemize}
\item btBoxShape: caja definida por el tamaño de sus lados.
\item btSphereShape: esfera definida por su radio.
\item btCapsuleShape: capsula alrededor del eje Y. Tambien existen btCapsuleShapeX/Z
\item btCylinderShape:
\item btConeShape: cono alrededor del eje Y. Tambien existen
  btConeShapeX/Z.
\item btMultiSphereShap: cascarón convexo formado a partir de varias
  esferas que puede ser usado para crear una capsula( a partir de dos
  esferas) u otras formas convexas.
\end{itemize}

Bullet tambien ofrece formas compuestas, pudiendo combinar múltiples
formas convexas en una única usando la clase
\emph{btCompoundShape}. Cada una de las formas que forman la malla
principal se llama \emph{forma hija}. Cada \emph{forma hija}
tiene sus propias transformaciones locales, relativas al
btCompoundShape. Existen algunas formas de colisión mas avanzadas que
permiten ajustarse a geometrías que no corresponden con formas
primitivas. Para mas información, se aconseja consultar el manual
oficial de Bullet~\cite{manual-bullet}\\

En la figura~\ref{fig:esquema-formas} siguiente se dan unas pautas
para seleccionar la forma
de colisión adecuada dependiendo de los requisitos del problema.\\

\begin{figure}[h]
  \centering
  \includegraphics[width=11cm]{figs/esquema-formas.png}
  \caption{Pipeline del componente de cuerpos }
  \label{fig:esquema-formas}
\end{figure}


Con lo anterior, ya estamos en condiciones de crear cuerpos rígidos
con sus correspondientes formas de colisión esféricas. El siguiente
paso consiste en renderizar los cuerpos. Cómo dijimos al principio,
usaremos Ogre3D para esta labor; sin embargo, nos surge un
problema. Si no comunicamos de alguna forma Ogre3D con Bullet, los
cuerpos gráficos creados en Ogre no se comportarán con el realismo que
les aporta Bullet. Para esto necesitamos hacer uso de la clase \emph{MotionState}.\\


\section{Integración con Ogre3D}
La clase MotionState nos permite actualizar la posición de los nodos
de escena de Ogre. Para ello, necesitamos implementarla, dado que se
trata de una clase abstracta. La implementación de la
clase~\cite{motion-state}  podemos verla en el siguiente listado:

\begin{listing}[style=C++]
  class MyMotionState : public btMotionState
{
protected:
    Ogre::SceneNode* mSceneNode;
    btTransform mInitialPosition;

public:
    MyMotionState(const btTransform &initialPosition, Ogre::SceneNode *node)
    {
        mSceneNode = node;
	mInitialPosition = initialPosition;
    }

    virtual ~MyMotionState()
    {
    }

    void setNode(Ogre::SceneNode *node)
    {
        mSceneNode = node;
    }

    virtual void getWorldTransform(btTransform &worldTrans) const
    {
        worldTrans = mInitialPosition;
    }

    virtual void setWorldTransform(const btTransform &worldTrans)
    {
        if(mSceneNode == nullptr)
            return; // silently return before we set a node

        btQuaternion rot = worldTrans.getRotation();
        mSceneNode ->setOrientation(rot.w(), rot.x(), rot.y(), rot.z());
        btVector3 pos = worldTrans.getOrigin();
        mSceneNode ->setPosition(pos.x(), pos.y(), pos.z());
    }
};
\end{listing}

La clase tiene como únicos atributos un objeto
btTransform de Bullet y un puntero a SceneNode de Ogre. Es
dentro del método \emph{setWorldTransform{const btTransform
    &worldTrans}} donde se realiza la actualización del nodo de
Ogre3D. La clase btTransform~\cite{btTransform} guarda un cuaternio que
  informa de las rotaciones del cuerpo y un vector que informa de la
  posición de este. Dado que la clase \emph{MotionState} almacena un puntero a un
nodo de escena de Ogre3D, se actualiza su rotación y posición a partir
de la referencia que se recibe de tipo btTransform como parámetro del
método.\\

Con este simple fragmento de código, es posible conectar Ogre3D con
Bullet, delegándole las operaciones de bajo nivel a este último.\\

\section{Conclusiones}
En este documento hemos visto como integrar Bullet junto con Ogre3D,
de forma que hemos creado un ejemplo muy sencillo que nos permite ver
los elementos básicos de Bullet.\\

\newpage
\addcontentsline{toc}{section}{Bibliografía}
\bibliographystyle{plain}
\begin{thebibliography}{4}
\bibitem{bullet-web}Página oficial del proyecto Bullet:
  \url{http://bulletphysics.org/wordpress/}
\bibitem{bullet-repo}Repositorio oficial del proyecto Bullet:
  \url{https://github.com/bulletphysics/bullet3}
\bibitem{bullet-chrome}Bullet usa WebGL:
  \url{http://bulletphysics.org/wordpress/?p=333}
\bibitem{bullet-nasa} La NASA usa Bullet:
  \url{http://bulletphysics.org/wordpress/?p=413}
\bibitem{ragdoll}Pagina wikipedia Ragdoll:
  \url{http://es.wikipedia.org/wiki/F%C3%ADsica_ragdoll}
\bibitem{repo-ejemplo} Repositorio del proyecto:
  \url{git@bitbucket.org:IsaacLacoba/intro-bullet.git}
\bibitem{motion-state} Página wiki Bullet. Motion State:
  \url{http://www.bulletphysics.org/mediawiki-1.5.8/index.php/MotionStates}
\bibitem{btTransform} Clase btTransform:
  \url{http://bulletphysics.org/Bullet/BulletFull/classbtTransform.html}
\bibitem{web-ois} Página de SourceForge de la biblioteca OIS:
  \url{http://sourceforge.net/projects/wgois/}
\bibitem{manual-bullet} Manual oficial Bullet:
  \url{http://www.cs.uu.nl/docs/vakken/mgp/assignment/Bullet%20-%20User%20Manual.pdf}
\end{thebibliography}
\end{document}
\grid
\grid
