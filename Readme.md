# Introducción a Bullet Physics

Este repositorio contiene un sencillo tutorial acerca de Bullet.

Aunque las explicaciones en este tutorial se han centrado en un sistema GNU/Linux, son perfectamente aplicables a un sistema Windows que tenga instalado Ogre3D y Bullet.

Si lo prefiere, dispone de un pdf con la documentación en la sección de descargas de este repositorio.

Cualquier sugerencia, contactarme por:

* ** gmail**: isaac.lacoba@gmail.com
* **twitter**: @ IsaacLacoba
